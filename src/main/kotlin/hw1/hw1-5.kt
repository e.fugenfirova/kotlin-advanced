package hw1

fun main() {
    val person1 = config( // с необязательными параметрами
        name = "Иван",
        surname = "Иванов",
        patronymic = "Иванович",
        gender = "мужской",
        dateOfBirth = "01.02.1985",
        age = 38,
        inn = "2934029502",
        snils = "123-456-789 01"
    )

    val person2 = config( // без них
        name = "Мария",
        surname = "Смирнова",
        gender = "женский",
        dateOfBirth = "01.02.1990",
        age = 33
    )

    val person3 = config( // В произвольном порядке следования параметров.
        surname = "Петров",
        name = "Петр",
        gender = "мужской",
        snils = "123-456-789 02",
        age = 0,
        dateOfBirth = "00-00-0000"
    )
}

fun config(name: String, surname: String, patronymic: String? = null, gender: String, dateOfBirth: String, age: Int,
           inn: String? = null, snils: String? = null) {
    println("name = $name, surname = $surname, patronymic = $patronymic, gender = $gender, dateOfBirth = $dateOfBirth, age = $age, " +
        "inn = $inn, snils = $snils")
}
/**
Напиши функцию, которая конфигурирует данные человека следующими параметрами:

имя
фамилия
отчество
пол
дата рождения
возраст
ИНН
СНИЛС

Затем напиши несколько вызовов этой функции:

С обязательными параметрами и без них.
В произвольном порядке следования параметров.
 **/