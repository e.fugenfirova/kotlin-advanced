package hw1

fun main() {
    println(
        configPerson(
            name = "Мария",
            surname = "Смирнова",
            gender = "женский",
            dateOfBirth = "01.02.1990",
            age = 33
        )
    )

}

data class Person(
    val name: String,
    val surname: String,
    val patronymic: String? = null,
    val gender: String,
    val dateOfBirth: String,
    val age: Int,
    val inn: String? = null,
    val snils: String? = null
)

fun configPerson(
    name: String, surname: String, patronymic: String? = null, gender: String,
    dateOfBirth: String, age: Int, inn: String? = null, snils: String? = null
): Person {
    return Person(
        name = name,
        surname = surname,
        patronymic = patronymic,
        gender = gender,
        dateOfBirth = dateOfBirth,
        age = age,
        inn = inn,
        snils = snils
    )
}
