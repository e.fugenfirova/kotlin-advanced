package hw1

class Example {

    companion object {
        var counter = 0
        fun counter() {
            counter ++
            println("Вызван counter. Количество вызовов: $counter")
        }
    }
}

fun main() {
    Example.counter() // Вызван counter. Количество вызовов: 1
    Example.counter() // Вызван counter. Количество вызовов: 2
}
