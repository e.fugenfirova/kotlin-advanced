package hw1

fun main() {
    accept("12","122","1234", "fpo")
}

fun accept(vararg params: String) {
    println("Передано аргументов: ${params.size}")
    println(params.joinToString(separator = "; "))
}
